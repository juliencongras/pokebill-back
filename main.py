from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class ChasseurIN(BaseModel):
    nom: str
    age: int
    id: int

chasseurs = []

@app.post('/chasseur')
def add_chasseur(nouveau_chasseur: ChasseurIN, nom_chasseur: str, age_chasseur: int):

    #if(str.isalpha(nom_chasseur) == False): to check if There are ONLY letters (No spaces allowed)
    #if(str.isdigit(nom_chasseur)): to check if there are only numbers (Can still name yourself
    #"9 000" since space isn't a number).
    #The first one is too restrictive (No names like "Jean-Claude" allowed)
    #And the second one is basically useless so I don't think I'll filter anything
    #Go ahead and call yourself random numbers if you want

    nouveau_chasseur.nom = nom_chasseur
    if(age_chasseur < 0):
        return
    nouveau_chasseur.age = age_chasseur
    nouveau_chasseur.id = len(chasseurs)
    chasseurs.append(nouveau_chasseur)
    return nouveau_chasseur

@app.get('/chasseur')
def get_all_chasseurs():
    return chasseurs